const service = require('./service');

const app = {
    sum: function(a, b){
        return a + b;
    },
    apiCall: async function(){
        return await service.fancyCall();
    }
};

module.exports = app;