const fancyService = {
    fancyCall: async function(){
        const fetch = require("node-fetch");
        const url = "http://www.mocky.io/v2/5ec779e22f0000aa3d42714d";
        try {
            const response = await fetch(url);
            const text = await response.text();
            return text;
        } catch (error) {
            console.log(error);
        }
    }
};

module.exports = fancyService;
