const tape = require('tape');
const Proxyquire = require('proxyquire').noCallThru();
const serviceStub = require('./stubs');

var app = Proxyquire('./app', {
    './service': serviceStub
});

tape('adding 2 + 2', function (t) {
    const sum = app.sum(2, 2);
    t.equal(sum, 4, "basic test is working");
    t.end();
});

tape('stubbed fancy call', async function (t) {
    const response = await app.apiCall();
    t.equal(response, "tomato", "stubbed api call is working");
    t.end();
});