const tape = require('tape');
const sinon = require('sinon');
const app = require('./app');
const stubs = require('./stubs');

tape('adding 2 + 2', function (t) {
    const sum = app.sum(2, 2);
    t.equal(sum, 4, "basic test is working");
    t.end();
});

tape('real fancy call', async function (t) {
    const response = await app.apiCall();
    t.equal(response, "potato", "real api call is working");
    t.end();
});

tape('stubbing fancy call with sinon', async function (t) {

    sinon.stub(app, 'apiCall').callsFake(function(){
        return 'banana'
    })

    const response = await app.apiCall();
    t.equal(response, "banana", "we stubbed service call with our call");
    t.end();
});